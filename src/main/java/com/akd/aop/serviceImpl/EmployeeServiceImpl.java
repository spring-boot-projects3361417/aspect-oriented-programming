package com.akd.aop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akd.aop.entity.Employee;
import com.akd.aop.repository.EmployeeRepository;
import com.akd.aop.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public List<Employee> getAllEmployees() {
		
		return employeeRepository.findAll();
	}
	
	@Override
	public Employee saveEmployee(Employee employee) {
		
		return employeeRepository.saveAndFlush(employee);
	}
	
	@Override
	public Employee updateEmployee(Employee employee) {
		
		boolean isEmployeeExisting = employeeRepository.existsById(employee.getId());
		if(isEmployeeExisting) {
			return employeeRepository.saveAndFlush(employee);
		}else {
			throw new RuntimeException("No employee exists with id="+ employee.getId());
		}
		
		
	}
	
	@Override
	public String deleteEmployeeById(int id) {
		employeeRepository.deleteById(id);
		return "Employee deleted";
	}
	
	
}
