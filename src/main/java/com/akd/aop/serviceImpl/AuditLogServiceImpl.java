package com.akd.aop.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akd.aop.entity.AuditLog;
import com.akd.aop.repository.AuditLogRepository;
import com.akd.aop.service.AuditLogService;

@Service
public class AuditLogServiceImpl implements AuditLogService {
	
	@Autowired
	private AuditLogRepository auditLogRepository;
	
	@Override
	public AuditLog saveAuditLog(AuditLog auditLog) {
		return auditLogRepository.saveAndFlush(auditLog);
	}
	
	@Override
	public List<AuditLog> getAllAuditLogs() {
		return auditLogRepository.findAll();
	}

}
