package com.akd.aop.service;

import java.util.List;

import com.akd.aop.entity.AuditLog;

public interface AuditLogService {

	AuditLog saveAuditLog(AuditLog auditLog);

	List<AuditLog> getAllAuditLogs();

}