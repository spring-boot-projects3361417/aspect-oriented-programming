package com.akd.aop.service;

import java.util.List;

import com.akd.aop.entity.Employee;

public interface EmployeeService {

	List<Employee> getAllEmployees();

	Employee saveEmployee(Employee employee);

	String deleteEmployeeById(int id);

	Employee updateEmployee(Employee employee);

}