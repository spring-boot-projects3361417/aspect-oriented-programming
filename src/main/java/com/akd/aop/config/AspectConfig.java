package com.akd.aop.config;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.akd.aop.entity.AuditLog;
import com.akd.aop.entity.Employee;
import com.akd.aop.service.AuditLogService;

@Configuration
@Aspect
public class AspectConfig {

	@Autowired
	private AuditLogService auditLogService;
	
	
	@AfterThrowing("execution(public * com.akd.aop.serviceImpl.EmployeeServiceImpl.updateEmployee(..))")
	public void afterInsert(JoinPoint joinPoint) {
		Employee emp = (Employee) joinPoint.getArgs()[0];
		AuditLog auditLog = new AuditLog();
		auditLog.setCreationDate(new Date());
		auditLog.setLog("Error encountered while updating employee with id=" + emp.getId());
		auditLogService.saveAuditLog(auditLog);
	}
}
